/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.domain.users.usecase

import android.annotation.SuppressLint
import com.sergio.codewars.domain.users.repository.UserRepository
import com.sergio.codewars.domain.users.utils.Interactor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class GetUsersInteractor @Inject constructor(
        private val usersRepository: UserRepository) : Interactor<GetUsersRequest, GetUsersOutput>() {

    @SuppressLint("RxLeakedSubscription")
    override fun execute(request: GetUsersRequest, output: GetUsersOutput): Disposable =
            if (request.username != null) {
                usersRepository.getUsers(request.username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(output::onUsersSuccess,
                        output::onUnknownError)
            } else {
                usersRepository.getUsers(null)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(output::onUsersSuccess,
                                output::onUnknownError)
            }
}