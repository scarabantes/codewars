/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.model

import com.google.gson.annotations.SerializedName


data class UserModel(
        @SerializedName("username") val username: String,
        @SerializedName("name") val name: String?,
        @SerializedName("ranks") val ranks: RanksModel)

data class RanksModel(@SerializedName("overall") val overallRankModel: OverallRankModel,
                      @SerializedName("languages") val languagesModel: LanguagesModel)

data class OverallRankModel(@SerializedName("rank") val rank: Int,
                            @SerializedName("name") val name: String,
                            @SerializedName("score") val score: Int)

data class LanguagesModel(@SerializedName("ruby") val ruby: LanguageRankModel?,
                          @SerializedName("c#") val c: LanguageRankModel?,
                          @SerializedName(".net") val net: LanguageRankModel?,
                          @SerializedName("javascript") val javascript: LanguageRankModel?,
                          @SerializedName("coffeescript") val coffeescript: LanguageRankModel?,
                          @SerializedName("node") val node: LanguageRankModel?,
                          @SerializedName("rails") val rails: LanguageRankModel?)

data class LanguageRankModel(@SerializedName("rank") val rank: Int,
                             @SerializedName("name") val name: String,
                             @SerializedName("color") val color: String,
                             @SerializedName("score") val score: Int)