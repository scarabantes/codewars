/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.database

import com.sergio.codewars.data.mapper.UserCachedEntityMapper
import com.sergio.codewars.data.model.LanguagesEntity
import com.sergio.codewars.data.UserCache
import com.sergio.codewars.domain.users.model.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single


class UsersCacheImpl (val userDatabase: UsersDatabase,
                      private val entityMapper: UserCachedEntityMapper) : UserCache {

    override fun saveUsers(users: List<User>): Completable {
        return Completable.defer {
            users.forEach {
                user -> userDatabase.userDao().insertUser(entityMapper.mapToCached(user))
                user.languages.forEach {
                    userDatabase.userDao().insertLanguage(LanguagesEntity(user.username, it.languageName, it.score))
                }
            }
            Completable.complete()
        }
    }

    override fun getUsers(): Flowable<List<User>> {
        return Flowable.defer {
            Flowable.just(userDatabase.userDao().getUsersWithLanguages())
        }.map {
            it.map { entityMapper.mapFromCached(it) }
        }
    }

    override fun isCached(): Single<Boolean> {
        return Single.defer {
            Single.just(userDatabase.userDao().getUsers().isNotEmpty())
        }
    }

    override fun setLastCacheTime(lastCache: Long) {
        //no operation for now.
    }

    override fun isExpired(): Boolean {
        return true //it is always expired
    }

}