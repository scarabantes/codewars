/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data

import com.sergio.codewars.data.source.UserDataStoreFactory
import com.sergio.codewars.domain.users.model.ChallengesData
import com.sergio.codewars.domain.users.model.User
import com.sergio.codewars.domain.users.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject


class UserDataRepository @Inject constructor(private val factory: UserDataStoreFactory): UserRepository {

    override fun saveUsers(users: List<User>): Completable {
        return factory.retrieveCacheDataStore().saveUsers(users)
    }

    override fun getUsers(username: String?): Flowable<List<User>> {
        if (username == null) {
            return factory.retrieveCacheDataStore().getUsers(null)
        }
        return factory.retrieveRemoteDataStore().getUsers(username)
                .flatMap {
                    saveUsers(it).toSingle { it }.flatMapPublisher {
                        factory.retrieveCacheDataStore().getUsers(null)
                    }
                }
    }

    override fun getCompletedChallenges(username: String): Flowable<ChallengesData> {
        return factory.retrieveRemoteDataStore().getCompletedChallenges(username)
    }
}