/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.mapper

import com.sergio.codewars.data.model.LanguagesModel
import com.sergio.codewars.data.model.UserModel
import com.sergio.codewars.domain.users.model.Language
import com.sergio.codewars.domain.users.model.Rank
import com.sergio.codewars.domain.users.model.User
import javax.inject.Inject


open class UserRemoteEntityMapper @Inject constructor(): EntityRemoteMapper<UserModel, User> {

    override fun mapFromRemote(from: UserModel): User =
            User(
                    username = from.username,
                    name = from.name,
                    rank = Rank(
                            from.ranks.overallRankModel.rank,
                            from.ranks.overallRankModel.name,
                            from.ranks.overallRankModel.score),
                    languages = mapLanguages(from.ranks.languagesModel))

    private fun mapLanguages(from: LanguagesModel): List<Language> {
        val lang: ArrayList<Language> = ArrayList()
        from.ruby?.let { lang.add(Language("ruby", from.ruby.score)) }
        from.c?.let { lang.add(Language("c", from.c.score)) }
        from.net?.let { lang.add(Language(".net", from.net.score)) }
        from.javascript?.let { lang.add(Language("javascript", from.javascript.score)) }
        from.coffeescript?.let { lang.add(Language("coffeescript", from.coffeescript.score)) }
        from.node?.let { lang.add(Language("node", from.node.score)) }
        from.rails?.let { lang.add(Language("rails", from.rails.score)) }
        return lang
    }
}