/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.mapper

import com.sergio.codewars.data.model.ChallengesModelData
import com.sergio.codewars.domain.users.model.Challenges
import com.sergio.codewars.domain.users.model.ChallengesData
import javax.inject.Inject


open class ChallengesRemoteEntityMapper @Inject constructor():
        EntityRemoteMapper<ChallengesModelData, ChallengesData> {

    override fun mapFromRemote(from: ChallengesModelData): ChallengesData {
        val challenges: ArrayList<Challenges> = ArrayList()
        from.challenges.forEach {
            challenges.add(Challenges(it.name))
        }
        return ChallengesData(challenges)
    }
}