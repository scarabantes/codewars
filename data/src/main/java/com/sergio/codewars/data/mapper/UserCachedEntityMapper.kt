/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.mapper

import com.sergio.codewars.data.model.LanguagesEntity
import com.sergio.codewars.data.model.RanksEntity
import com.sergio.codewars.data.model.UserAndLanguages
import com.sergio.codewars.data.model.UserEntity
import com.sergio.codewars.domain.users.model.Language
import com.sergio.codewars.domain.users.model.Rank
import com.sergio.codewars.domain.users.model.User
import javax.inject.Inject


open class UserCachedEntityMapper @Inject constructor() {


    fun mapToCached(from: User): UserEntity {
        return UserEntity(
                from.username,
                from.name,
                RanksEntity(from.rank.rank, from.rank.name, from.rank.score))
    }

    fun mapFromCached(from: UserAndLanguages): User {
        return User(
                from.userEntity.username,
                from.userEntity.name,
                Rank(from.userEntity.ranksEntity.rank, from.userEntity.ranksEntity.name, from.userEntity.ranksEntity.score),
                mapFromLanguagesCached(from.languagesEntityList))
    }

    private fun mapFromLanguagesCached(languagesEntityList: List<LanguagesEntity>): List<Language> {
        val languages: MutableList<Language> = ArrayList()
        languagesEntityList.forEach {
            languages.add(Language(it.languageName, it.score))
        }
        return languages
    }
}