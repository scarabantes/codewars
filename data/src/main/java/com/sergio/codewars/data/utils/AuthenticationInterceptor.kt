/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.utils

import com.sergio.codewars.data.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject


class AuthenticationInterceptor @Inject
constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        request = request.newBuilder()
                .addHeader(AUTHORIZATION_HEADER_KEY, BuildConfig.API_CODEWARS_KEY)
                .addHeader(AUTHORIZATION_HEADER_CONTENT_TYPE_KEY, AUTHORIZATION_HEADER_CONTENT_TYPE_VALUE)
                .build()

        return chain.proceed(request)
    }

    companion object {

        private val AUTHORIZATION_HEADER_KEY = "Authorization"
        private val AUTHORIZATION_HEADER_CONTENT_TYPE_KEY = "Content-type"
        private val AUTHORIZATION_HEADER_CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded;charset=UTF-8"
    }

}