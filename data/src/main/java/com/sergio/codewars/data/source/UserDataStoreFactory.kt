/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.source

import com.sergio.codewars.data.UserDataStore
import javax.inject.Inject


open class UserDataStoreFactory @Inject constructor(
        private val userCacheDataStore: UserCacheDataStore,
        private val userRemoteDataStore: UserRemoteDataStore) {

    open fun retrieveCacheDataStore(): UserDataStore {
        return userCacheDataStore
    }

    open fun retrieveRemoteDataStore(): UserDataStore {
        return userRemoteDataStore
    }

}