/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.source

import com.sergio.codewars.data.UserCache
import com.sergio.codewars.data.UserDataStore
import com.sergio.codewars.domain.users.model.ChallengesData
import com.sergio.codewars.domain.users.model.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject


open class UserCacheDataStore @Inject constructor(private val userCache: UserCache) : UserDataStore {
    override fun getCompletedChallenges(username: String): Flowable<ChallengesData> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveUsers(users: List<User>): Completable {
        return userCache.saveUsers(users)
                .doOnComplete { userCache.setLastCacheTime(System.currentTimeMillis()) }
    }

    override fun getUsers(username: String?): Flowable<List<User>> {
        return userCache.getUsers()
    }

    override fun isCached(): Single<Boolean> {
        return userCache.isCached()
    }
}