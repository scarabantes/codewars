/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.challenges.factory

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.sergio.codewars.data.challenges.datasource.CompletedChallengesDataSource
import com.sergio.codewars.data.mapper.ChallengesRemoteEntityMapper
import com.sergio.codewars.data.remote.UsersService
import com.sergio.codewars.domain.users.model.Challenges
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CompletedChallengesFactory @Inject constructor(private val usersService: UsersService,
                                                     private val challengesChallengesMapper: ChallengesRemoteEntityMapper,
                                                     private val compositeDisposable: CompositeDisposable) :
        DataSource.Factory<String, Challenges>() {

    private val usersDataSourceLiveData = MutableLiveData<CompletedChallengesDataSource>()
    private lateinit var username: String

    override fun create(): DataSource<String, Challenges> {
        val source = CompletedChallengesDataSource(usersService, challengesChallengesMapper, compositeDisposable, username)
        usersDataSourceLiveData.postValue(source)
        return source
    }

    fun setUserName(username: String) {
        this.username = username
    }

}
