/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.data.remote

import com.sergio.codewars.data.mapper.ChallengesRemoteEntityMapper
import com.sergio.codewars.data.mapper.UserRemoteEntityMapper
import com.sergio.codewars.data.UserRemote
import com.sergio.codewars.domain.users.model.ChallengesData
import com.sergio.codewars.domain.users.model.User
import io.reactivex.Flowable
import javax.inject.Inject


class UserRemoteImpl @Inject constructor(private val usersService: UsersService,
                                         private val entityMapper: UserRemoteEntityMapper,
                                         private val challengesChallengesMapper: ChallengesRemoteEntityMapper): UserRemote {

    override fun getUsers(username: String?): Flowable<List<User>> {
        return usersService.getUser(username)
                .map {
                    val entities = mutableListOf<User>()
                    entities.add(entityMapper.mapFromRemote(it))
                    entities
                }
    }

    override fun getCompletedChallenges(username: String): Flowable<ChallengesData> {
        return usersService.getCompletedChallenges(username, 0)
                .map {
                    challengesChallengesMapper.mapFromRemote(it)
                }
    }
}