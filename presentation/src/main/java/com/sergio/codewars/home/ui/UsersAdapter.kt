/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.home.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sergio.codewars.R
import com.sergio.codewars.extension.inflate
import kotlinx.android.synthetic.main.row_user.view.*
import javax.inject.Inject
import kotlin.properties.Delegates


class UsersAdapter @Inject constructor() : RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    internal var usersList: List<UserView> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    internal var clickListener: (UserView) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            UserViewHolder(parent.inflate(R.layout.row_user))

    override fun onBindViewHolder(userViewHolder: UserViewHolder, position: Int) =
            userViewHolder.bind(usersList[position], clickListener)

    override fun getItemCount() = usersList.size

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(userView: UserView, clickListener: (UserView) -> Unit) {
            itemView.username.text = userView.username
            itemView.rank.text = userView.rank
            itemView.language.text = userView.languageList.sortedBy { it.score }.last().languageName
            itemView.score.text = userView.languageList.sortedBy { it.score }.last().score.toString()
            itemView.setOnClickListener { clickListener(userView) }
        }
    }
}