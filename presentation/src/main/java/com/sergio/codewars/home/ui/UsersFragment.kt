/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.home.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.EditText
import com.sergio.codewars.common.BaseFragment
import com.sergio.codewars.R
import com.sergio.codewars.extension.invisible
import com.sergio.codewars.extension.observe
import com.sergio.codewars.extension.viewModel
import com.sergio.codewars.extension.visible
import com.sergio.codewars.home.viewmodel.UsersViewModel
import com.sergio.codewars.navigation.Navigator
import kotlinx.android.synthetic.main.empty.*
import kotlinx.android.synthetic.main.fragment_users.*
import javax.inject.Inject

class UsersFragment : BaseFragment() {

    @Inject lateinit var navigator: Navigator
    @Inject lateinit var usersAdapter: UsersAdapter

    private lateinit var usersViewModel: UsersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        setHasOptionsMenu(true)
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeRecyclerView()
        loadUsers()
    }

    private fun initializeRecyclerView() {
        usersList.layoutManager = LinearLayoutManager(context)
        usersList.adapter = usersAdapter
        usersAdapter.clickListener = { user -> navigator.showChallenges(activity!!, user.username) }
    }

    private fun loadUsers() {
        emptyLayout.invisible()
        progressBar.visible()
        usersViewModel.loadUsers(null)
    }

    private fun initViewModel() {
        usersViewModel = viewModel(viewModelFactory) {
            observe(users, ::renderUserList)
        }
    }

    override fun layoutId() = R.layout.fragment_users

    private fun renderUserList(userList: List<UserView>?) {
        progressBar.invisible()
        if (userList!!.isNotEmpty()) {
            emptyLayout.invisible()
        } else {
            emptyLayout.visible()
        }
        usersAdapter.usersList = userList.orEmpty()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        initSearchView(menu)
    }

    private fun initSearchView(menu: Menu) {
        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.queryHint = getString(R.string.search_hint)
        val searchEditText = searchView.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
        searchEditText.setTextColor(resources.getColor(R.color.colorBackground))
        searchEditText.setHintTextColor(resources.getColor(R.color.colorBackground))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(username: String): Boolean {
                progressBar.visible()
                emptyLayout.invisible()
                usersViewModel.loadUsers(username)
                searchView.clearFocus()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

    }
}
