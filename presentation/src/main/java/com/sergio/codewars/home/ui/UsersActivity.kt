/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.home.ui

import android.os.Bundle
import com.sergio.codewars.R
import com.sergio.codewars.common.BaseActivity
import com.sergio.codewars.extension.inTransaction
import kotlinx.android.synthetic.main.toolbar.*


class UsersActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout)
        setSupportActionBar(toolbar)
        addFragment()
    }

    private fun addFragment() {
        supportFragmentManager.inTransaction {
            add( R.id.fragment_container, UsersFragment()) }
    }
}
