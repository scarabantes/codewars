/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.home.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sergio.codewars.domain.users.model.User
import com.sergio.codewars.domain.users.usecase.GetUsersInteractor
import com.sergio.codewars.domain.users.usecase.GetUsersOutput
import com.sergio.codewars.domain.users.usecase.GetUsersRequest
import com.sergio.codewars.home.ui.UserView
import timber.log.Timber
import javax.inject.Inject


class UsersViewModel
@Inject constructor(private val getGetUsersInteractor: GetUsersInteractor): ViewModel() {

    var users: MutableLiveData<List<UserView>> = MutableLiveData()

    fun loadUsers(username: String?) = getGetUsersInteractor.execute(GetUsersRequest(username), GetUsersOutputImpl())

    inner class GetUsersOutputImpl: GetUsersOutput {
        override fun onUsersSuccess(user: List<User>) {
            Timber.i("info user: $user")
            users.value = user.map { UserView(it.username, it.rank.name, it.languages) }

        }

        override fun onUnknownError(throwable: Throwable) {
            Timber.i("info user error: $throwable")
        }

    }
}