/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.navigation

import android.content.Intent
import android.support.v4.app.FragmentActivity
import com.sergio.codewars.challenges.ui.ChallengesActivity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Navigator
@Inject constructor() {

    fun showChallenges(activity: FragmentActivity, username: String) {
        val intent = Intent(activity, ChallengesActivity::class.java)
        intent.putExtra(EXTRA_USERNAME, username)
        activity.startActivity(intent)
    }

    companion object {
        const val EXTRA_USERNAME = "EXTRA_USERNAME"
    }
}


