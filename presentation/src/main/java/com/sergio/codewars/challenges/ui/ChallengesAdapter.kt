/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.challenges.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sergio.codewars.R
import com.sergio.codewars.domain.users.model.Challenges
import com.sergio.codewars.extension.inflate
import kotlinx.android.synthetic.main.row_completed_challenges.view.*
import javax.inject.Inject
import kotlin.properties.Delegates


class ChallengesAdapter @Inject constructor() : PagedListAdapter<Challenges, ChallengesAdapter.ChallengesViewHolder>(DIFF_CALLBACK) {

    internal var challengesList: List<Challenges> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    internal var clickListener: (Challenges) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ChallengesViewHolder(parent.inflate(R.layout.row_completed_challenges))

    override fun onBindViewHolder(challengesViewHolder: ChallengesViewHolder, position: Int) {
        challengesViewHolder.bind(challengesList[position], clickListener)
    }

    override fun getItemCount() = challengesList.size

    class ChallengesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(challengesView: Challenges, clickListener: (Challenges) -> Unit) {
            itemView.challengeName.text = challengesView.name
            itemView.setOnClickListener { clickListener(challengesView) }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Challenges>() {
            override fun areItemsTheSame(oldChallenges: Challenges,
                                         newChallenges: Challenges): Boolean =
                    oldChallenges.name == newChallenges.name

            override fun areContentsTheSame(oldConcert: Challenges,
                                            newConcert: Challenges): Boolean =
                    oldConcert == newConcert
        }
    }
}