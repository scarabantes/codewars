/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.challenges.ui

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.MenuItem
import com.sergio.codewars.R
import com.sergio.codewars.common.BaseActivity
import com.sergio.codewars.extension.inTransaction
import kotlinx.android.synthetic.main.toolbar.*


class ChallengesActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenges)
        configureToolbar()
        setBottomNavigation()
        showCompletedChallenges()
    }

    private fun configureToolbar() {
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back)
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setBottomNavigation() {
        val navigation = findViewById<BottomNavigationView>(R.id.navigationView)
        navigation.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_completed -> {
                showCompletedChallenges()
                return true
            }
            R.id.navigation_authored -> {
                showAuthoredChallenges()
                return true
            }
        }
        return false
    }

    private fun showCompletedChallenges() {
        supportFragmentManager.inTransaction { replace(
                R.id.fragment_container, CompletedChallengesFragment()) }
    }

    private fun showAuthoredChallenges() {
        supportFragmentManager.inTransaction { replace(
                R.id.fragment_container, AuthoredChallengesFragment()) }
    }
}
