/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.challenges.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.sergio.codewars.R
import com.sergio.codewars.challenges.viewmodel.AuthoredChallengesViewModel
import com.sergio.codewars.common.BaseFragment
import com.sergio.codewars.domain.users.model.Challenges
import com.sergio.codewars.extension.invisible
import com.sergio.codewars.extension.observe
import com.sergio.codewars.extension.viewModel
import com.sergio.codewars.extension.visible
import com.sergio.codewars.navigation.Navigator
import kotlinx.android.synthetic.main.empty.*
import kotlinx.android.synthetic.main.fragment_challenges.*
import javax.inject.Inject

class AuthoredChallengesFragment : BaseFragment() {

    @Inject lateinit var challengesAdapter: ChallengesAdapter
    private lateinit var authoredChallengesViewModel: AuthoredChallengesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        setHasOptionsMenu(true)
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeRecyclerView()
    }

    private fun initializeRecyclerView() {
        completedList.layoutManager = LinearLayoutManager(context)
        completedList.adapter = challengesAdapter
        progressBar.visible()
    }

    private fun initViewModel() {
        authoredChallengesViewModel = viewModel(viewModelFactory) {}
        authoredChallengesViewModel.loadData(activity!!.intent.getStringExtra(Navigator.EXTRA_USERNAME))
        observe(authoredChallengesViewModel.challenges, ::renderUserList)
    }

    override fun layoutId() = R.layout.fragment_challenges

    private fun renderUserList(userList: List<Challenges>?) {
        progressBar.invisible()
        if (userList!!.isEmpty()) {
            emptyLayout.visible()
        }
        challengesAdapter.challengesList = userList.orEmpty()
    }
}
