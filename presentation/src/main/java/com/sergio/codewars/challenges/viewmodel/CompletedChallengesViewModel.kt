/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.challenges.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.sergio.codewars.data.challenges.factory.CompletedChallengesFactory
import com.sergio.codewars.domain.users.model.Challenges
import javax.inject.Inject


class CompletedChallengesViewModel
@Inject constructor(private val completedChallengesFactory: CompletedChallengesFactory): ViewModel() {

    var challenges: LiveData<PagedList<Challenges>> = MutableLiveData()

    fun loadData(userName: String) {
        completedChallengesFactory.setUserName(userName)
        challenges = LivePagedListBuilder<String, Challenges>(completedChallengesFactory, config).build()
    }
    companion object {
        private const val PAGE_SIZE = 20
        private const val PREFETCH_DISTANCE = 10
        private const val ENABLE_PLACEHOLDERS = false
        private val config = PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setEnablePlaceholders(ENABLE_PLACEHOLDERS)
                .build()
    }
}