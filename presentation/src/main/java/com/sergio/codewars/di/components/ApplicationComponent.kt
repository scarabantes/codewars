/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.di.components

import com.sergio.codewars.CodewarsApplication
import com.sergio.codewars.challenges.ui.AuthoredChallengesFragment
import com.sergio.codewars.challenges.ui.CompletedChallengesFragment
import com.sergio.codewars.di.modules.*
import com.sergio.codewars.home.ui.UsersFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ViewModelModule::class,
    ServicesModule::class,
    NetworkModule::class,
    RepositoryModule::class])
interface ApplicationComponent {

    fun inject(application: CodewarsApplication)

    fun inject(usersFragment: UsersFragment)

    fun inject(completedChallengesFragment: CompletedChallengesFragment)

    fun inject(authoredChallengesFragment: AuthoredChallengesFragment)
}
