/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.sergio.codewars.data.UserDataRepository
import com.sergio.codewars.data.database.UsersCacheImpl
import com.sergio.codewars.data.database.UsersDatabase
import com.sergio.codewars.data.mapper.ChallengesRemoteEntityMapper
import com.sergio.codewars.data.mapper.UserCachedEntityMapper
import com.sergio.codewars.data.mapper.UserRemoteEntityMapper
import com.sergio.codewars.data.remote.UserRemoteImpl
import com.sergio.codewars.data.remote.UsersService
import com.sergio.codewars.data.UserCache
import com.sergio.codewars.data.UserRemote
import com.sergio.codewars.data.source.UserDataStoreFactory
import com.sergio.codewars.domain.users.repository.UserRepository
import dagger.Module
import dagger.Provides


@Module
class RepositoryModule {

    @Provides internal fun provideUserRemote(service: UsersService,
                                             userRemoteEntityMapper: UserRemoteEntityMapper,
                                             challengesChallengesMapper: ChallengesRemoteEntityMapper): UserRemote =
            UserRemoteImpl(service, userRemoteEntityMapper, challengesChallengesMapper)


    @Provides internal fun provideUserCache(database: UsersDatabase,
                                            entityMapper: UserCachedEntityMapper): UserCache =
            UsersCacheImpl(database, entityMapper)


    @Provides internal fun provideUserRepository(factory: UserDataStoreFactory): UserRepository =
            UserDataRepository(factory)


    @Provides internal fun provideUsersDatabase(context: Context): UsersDatabase =
            Room.databaseBuilder(context, UsersDatabase::class.java, "codewars.db").build()
}