/*
 * Copyright (C) 2018 Sergio Carabantes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sergio.codewars.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sergio.codewars.challenges.viewmodel.AuthoredChallengesViewModel
import com.sergio.codewars.challenges.viewmodel.CompletedChallengesViewModel
import com.sergio.codewars.common.ViewModelFactory
import com.sergio.codewars.common.ViewModelKey
import com.sergio.codewars.home.viewmodel.UsersViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(UsersViewModel::class)
    abstract fun bindsMainViewModel(usersViewModel: UsersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompletedChallengesViewModel::class)
    abstract fun bindCompletedChallengesViewModel(completedChallengesViewModel: CompletedChallengesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AuthoredChallengesViewModel::class)
    abstract fun bindAuthoredChallengesViewModel(authoredChallengesViewModel: AuthoredChallengesViewModel): ViewModel
}